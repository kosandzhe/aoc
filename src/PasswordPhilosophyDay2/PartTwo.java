package PasswordPhilosophyDay2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class PartTwo {
    public static void main(String[] args) throws IOException {

        File file = new File("src\\PasswordPhilosophy");
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        int total = 0;
        while (line != null) {

            String[] two = line.split(" ");
            //System.out.println(Arrays.toString(two));

            String[] three = two[0].split("-");
            //System.out.println(Arrays.toString(three));

            String[] four = two[1].split(":");
            //System.out.println(Arrays.toString(four));

            String[] five = two[2].split("");
            //System.out.println(Arrays.toString(five));

            int a = Integer.parseInt(three[0]);
            int b = Integer.parseInt(three[1]);
            String character = four[0];

            if (five[a - 1].contains(character) ^ five[b - 1].contains(character)) {
                total++;
            }
            line = reader.readLine();
        }
        System.out.println("Total is: "+total);
    }
}
