package PasswordPhilosophyDay2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class PartOne {
    public static void main(String[] args) throws IOException {

        File file = new File("src\\PasswordPhilosophy");
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        int total = 0;
        while (line != null) {

            String[] two = line.split(" "); //                [8-11, l:, qllllqllklhlvtl]
            //System.out.println(Arrays.toString(two));

            String[] three = two[0].split("-");//            [8, 11]
            //System.out.println(Arrays.toString(three));

            String[] four = two[1].split(":");//             [l]
            //System.out.println(Arrays.toString(four));

            String[] five = two[2].split(""); //             [q, l, l, l, l, q, l, l, k, l, h, l, v, t, l]
            // System.out.println(Arrays.toString(five));

            int count = 0;
            String character = four[0];
            for (int i = 0; i < five.length; i++) {
                if (five[i].contains(character)) {
                    count++;
                }
            }

            int a = Integer.parseInt(three[0]);
            int b = Integer.parseInt(three[1]);

            if (count >= a & count <= b) {
                total++;
            }

            line = reader.readLine();
        }
        System.out.println("Total is: "+total);
    }
}
