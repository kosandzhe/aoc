package BinaryBoardingDay5;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PartOne {

    public static void main(String[] args) throws IOException {

        File file = new File("src\\BinaryBoarding");
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        List<Integer> list = new ArrayList<>();
        while (line != null) {

            String[] two = line.split("\\n");

            String wordNew = two[0].replaceAll("F", "0").substring(0, 7);
            String wnd = wordNew.replaceAll("B", "1").substring(0, 7);

            String razborEnd = two[0].replaceAll("R", "1").substring(7, 10);
            String razborEnd2 = razborEnd.replaceAll("L", "0").substring(0, 3);

            int chislo = Integer.parseInt(wnd, 2);
            int chislo2 = Integer.parseInt(razborEnd2, 2);

            int hoh = chislo * 8 + chislo2;


            list.add(hoh);
            line = reader.readLine();
        }
        System.out.println("Patr one: max value is = " + Collections.max(list));
        for (int i = 0; i < list.size(); i++) {
            if (!(list.contains(list.get(i) + 1))) {
                System.out.println("Part two: my set is = " + (list.get(i) + 1));
            }
        }
    }
}
