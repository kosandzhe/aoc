package PassportProcessingDay4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PartTwo {
    public static void main(String[] args) throws IOException {

        File file = new File("src\\PassportProcessing");
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        ArrayList<String[]> list = new ArrayList<>();
        while (line != null) {
            String[] one = line.split("\\n");
            list.add(one);
            line = reader.readLine();
        }

        String[] two = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            two[i] = Arrays.toString(list.get(i));
        }

        System.out.println(two[0]);
        System.out.println();

        String numbers = "1234567890";

        int byr = 0;
        int iyr = 0;
        int eyr = 0;
        int hgt = 0;
        int hcl = 0;
        int ecl = 0;
        int pid = 0;
        int countParam = 0;

        for (int x = 0; x < two.length; x++) {
            String[] three = two[x].split(" ");

            for (int i = 0; i < three.length; i++) {
                String[] four = three[i].split(":");

                if (!four[0].equals("[]")) {

                    if (four[0].contains("byr")) {
                        String number = four[1].substring(0, 4);
                        int date = Integer.parseInt(number);
                        if (date <= 2002 & date >= 1920) {
                            byr++;
                        }
                    }

                    if (four[0].contains("iyr")) {
                        String number = four[1].substring(0, 4);
                        int date = Integer.parseInt(number);
                        if (date <= 2020 & date >= 2010) {
                            iyr++;
                        }
                    }

                    if (four[0].contains("eyr")) {
                        String number = four[1].substring(0, 4);
                        int date = Integer.parseInt(number);
                        if (date <= 2030 & date >= 2020) {
                            eyr++;
                        }
                    }

                    int chislo;
                    if (four[0].contains("hgt")) {
                        if (four[1].contains("cm") | four[1].contains("in")) {
                            String[] razbor = four[1].split("");
                            String sum = "";
                            for (int a = 0; a < razbor.length; a++) {
                                if (numbers.contains(razbor[a])) {
                                    sum = sum + razbor[a];
                                }
                            }
                            chislo = Integer.parseInt(sum);
                            if (four[1].contains("cm")) {
                                if (chislo >= 150 & chislo <= 193) {
                                    hgt++;
                                }
                            }
                            if (four[1].contains("in")) {
                                if (chislo >= 59 & chislo <= 76) {
                                    hgt++;
                                }
                            }
                        }
                    }

                    String hclParam = "1234567890abcdef";
                    int inneHclCount = 0;
                    if (four[0].contains("hcl")) {
                        String[] razborChar = four[1].split("");
                        if (razborChar.length == 7) {
                            hcl = getHcl(hcl, hclParam, inneHclCount, razborChar);
                        } else if (razborChar.length == 8) {
                            if (razborChar[7].contains("]")) {
                                hcl = getHcl(hcl, hclParam, inneHclCount, razborChar);
                            }
                        }
                    }


                    if (four[0].contains("ecl")) {
                        if (four[1].equals("amb") || four[1].equals("blu") || four[1].equals("brn") || four[1].equals("gry") || four[1].equals("grn") || four[1].equals("hzl") || four[1].equals("oth")
                                || four[1].equals("amb]") || four[1].equals("blu]") || four[1].equals("brn]") || four[1].equals("gry]") || four[1].equals("grn]") || four[1].equals("hzl]") || four[1].equals("oth]")) {
                            ecl++;
                        }
                    }

                    int innerPidCount = 0;
                    String dadaPid = "1234567890";
                    if (four[0].contains("pid")) {
                        String[] razborChar = four[1].split("");
                        if (razborChar.length == 9) {
                            pid = getPid(pid, innerPidCount, dadaPid, razborChar);
                        } else if (razborChar.length == 10) {
                            if (razborChar[9].contains("]")) {
                                pid = getPid(pid, innerPidCount, dadaPid, razborChar);
                            }
                        }
                    }
                } else {
                    if ((byr + iyr + eyr + hgt + hcl + ecl + pid) == 7) {
                        countParam++;
                        byr = 0;
                        iyr = 0;
                        eyr = 0;
                        hgt = 0;
                        hcl = 0;
                        ecl = 0;
                        pid = 0;
                    } else {
                        byr = 0;
                        iyr = 0;
                        eyr = 0;
                        hgt = 0;
                        hcl = 0;
                        ecl = 0;
                        pid = 0;
                    }
                }
            }
        }

        System.out.println("countParam = " + countParam);

    }

    private static int getPid(int pid, int innerPidCount, String dadaPid, String[] razborChar) {
        for (int a = 0; a < razborChar.length; a++) {
            if (dadaPid.contains(razborChar[a])) {
                innerPidCount++;
                if (innerPidCount == 9) {
                    pid++;
                }
            }
        }
        return pid;
    }

    private static int getHcl(int hcl, String hclParam, int inneHclCount, String[] razborChar) {
        for (int q = 0; q < razborChar.length; q++) {
            if (hclParam.contains(razborChar[q])) {
                inneHclCount++;
                if (inneHclCount == 6) {
                    hcl++;
                }
            }
        }
        return hcl;
    }
}


