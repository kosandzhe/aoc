package PassportProcessingDay4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class PartOne {
    public static void main(String[] args) throws IOException {

        File file = new File("src\\PassportProcessing");
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        ArrayList<String[]> list = new ArrayList<>();
        while (line != null) {
            String[] one = line.split("\\n");
            list.add(one);
            line = reader.readLine();
        }

        String[] two = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            two[i] = Arrays.toString(list.get(i));
        }
        int count = 0;
        for (int x = 0; x < two.length; x++) {
            int innerCount = 0;
            while (innerCount <= 7) {
                if (!two[x].equals("[]")) {
                    if (two[x].contains("eyr")) {
                        innerCount++;
                    }
                    if (two[x].contains("pid")) {
                        innerCount++;
                    }
                    if (two[x].contains("hcl")) {
                        innerCount++;
                    }
                    if (two[x].contains("byr")) {
                        innerCount++;
                    }
                    if (two[x].contains("iyr")) {
                        innerCount++;
                    }
                    if (two[x].contains("ecl")) {
                        innerCount++;
                    }
                    if (two[x].contains("cid")) {
                        innerCount++;
                    }
                    if (two[x].contains("hgt")) {
                        innerCount++;
                    }
                    x++;
                    if (x == 1121) {
                        break;
                    }

                } else
                    break;
            }
            if (innerCount == 7) {
                count++;
            }
        }
        System.out.println("total: "+count);
    }
}
