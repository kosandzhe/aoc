package TobogganTrajectoryDay3;

import java.io.*;
import java.util.Arrays;

public class PartOneTwo {
    public static void main(String[] args) throws IOException {
        // https://adventofcode.com/2020/day/3
        File file = new File("src\\TobogganTrajectory");
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        int position = 0;
        int count = 0;
        while (line != null) {
            StringBuilder b = new StringBuilder(32 * line.length());
            for (int i = 0; i < 32; ++i) {
                b.append(line);
            }
            String result = b.toString();
            String[] one = result.split("");
            if (position != 0) {
                for (int y = position; y < position + 1; y++) {
                    if (one[y].contains(".")) {
                        one[y] = "O";
                    } else if (one[y].contains("#")) {
                        one[y] = "X";
                        count++;
                    }
                }
            }
            for (int i = position; i < one.length; i++) {
                if (i == 1 + position) {
                    position = i;
                    break;
                }
            }
            System.out.println(Arrays.toString(one));
            line = reader.readLine();

        }
        System.out.println("Total " + count);
    }
}
